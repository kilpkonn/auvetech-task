FROM ros:noetic-perception AS build

RUN apt-get update && apt-get upgrade -y
RUN apt-get install ros-noetic-cv-bridge \
    ros-noetic-video-stream-opencv -y

WORKDIR /catkin_ws

ADD src /catkin_ws/src


RUN chmod +x /opt/ros/noetic/setup.sh \
  && . /opt/ros/noetic/setup.sh \
  && pwd && ls && catkin_make

# Add custom ros entrypoint that sources our packages
ADD docker/ros_entrypoint.sh /ros_entrypoint.sh

ENTRYPOINT ["/ros_entrypoint.sh"]
CMD ["roslaunch", "traffic_light_fetcher", "traffic_light_fetcher.launch"]

