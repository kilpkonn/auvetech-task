# Traffic light fetcher
_Tavo Annus_

## Building
```bash
docker build -t auvetech:tlf .
```

## Running
Run container with
```bash
docker run -it auvetech:tlf --network host
```
Listen to topic
```bash
rostopic echo /zone_height
```
Feed video to container _(Can be ran on host machine)_
```bash
roslaunch traffic_light_fetcher fakecam.launch
```
You should now see video fed to node as well as output of `/zone_height`.

### Create compilation database
_Useful for LSP support for your editor_
```
jq -s 'map(.[])' ~/catkin_ws/build/**/compile_commands.json > compile_commands.json
```
