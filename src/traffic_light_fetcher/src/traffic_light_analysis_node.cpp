#include <ros/ros.h>
#include <std_msgs/Bool.h>

#include "traffic_light_fetcher/traffic_light_analysis.hpp"

int main(int argc, char *argv[]) {
  ros::init(argc, argv, "traffic_light_analysis");
  ros::NodeHandle n;

  traffic_light_fetcher::TrafficLightAnalysis controller(n);

  ros::Timer controllerTimer = n.createTimer(
      ros::Duration(0.1), &traffic_light_fetcher::TrafficLightAnalysis::step,
      &controller);

  ros::spin();

  return 0;
}
