#include <chrono>
#include <ros/ros.h>

#include "traffic_light_fetcher/traffic_light_fetcher.hpp"

int main(int argc, char *argv[]) {
  ros::init(argc, argv, "traffic_light_fetcher");
  ros::NodeHandle n;

  traffic_light_fetcher::TrafficLightFetcher controller(n);
  ros::Timer controllerTimer = n.createTimer(
      ros::Duration(1.0), &traffic_light_fetcher::TrafficLightFetcher::step,
      &controller);

  ros::spin();

  return 0;
}
