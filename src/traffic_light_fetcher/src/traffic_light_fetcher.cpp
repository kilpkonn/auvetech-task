#include <traffic_light_fetcher/traffic_light_fetcher.hpp>

#include <cv_bridge/cv_bridge.h>
#include <fstream>
#include <geometry_msgs/Vector3.h>
#include <iostream>
#include <opencv2/dnn.hpp>
#include <opencv2/dnn/all_layers.hpp>
#include <opencv2/opencv.hpp>
#include <ros/package.h>
#include <std_msgs/Bool.h>

namespace traffic_light_fetcher {
TrafficLightFetcher::TrafficLightFetcher(ros::NodeHandle nh) : nh_{nh} {
  videoSub_ = nh_.subscribe("/cam/image_raw", 1,
                            &TrafficLightFetcher::videoCallback, this);
  trafficLightDetectedPub_ =
      nh_.advertise<std_msgs::Bool>("/traffic_light_detected", 10);
  trafficLightSizePub_ =
      nh_.advertise<geometry_msgs::Vector3>("/traffic_light_size", 10);

  debug_ = nh_.param("/debug", false);
  confidenceThreshld_ =
      nh_.param("traffic_light_fetcher/confidence_threshold", 0.5);

  std::string mapperPackagePath =
      ros::package::getPath("traffic_light_fetcher");
  // Models are from:
  // https://github.com/rdeepc/ExploreOpencvDnn/tree/master/models
  std::string pathGraph = mapperPackagePath + "/data/frozen_inference_graph.pb";
  std::string pathNet =
      mapperPackagePath + "/data/ssd_mobilenet_v2_coco_2018_03_29.txt";

  nn_ = cv::dnn::readNet(pathGraph, pathNet, "TensorFlow");
  nn_.setPreferableBackend(cv::dnn::DNN_BACKEND_CUDA);
  nn_.setPreferableTarget(cv::dnn::DNN_TARGET_CUDA);
}

void TrafficLightFetcher::step(const ros::TimerEvent &event) {
  if (!frame_)
    return;

  cv_bridge::CvImageConstPtr cvPtr;
  try {
    namespace enc = sensor_msgs::image_encodings;
    cvPtr = cv_bridge::toCvShare(frame_, enc::BGR8);
  } catch (cv_bridge::Exception &e) {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }
  cv::Mat blob =
      cv::dnn::blobFromImage(cvPtr->image, 1.0, cv::Size(300, 300),
                             cv::Scalar(127.5, 127.5, 127.5), true, false);
  nn_.setInput(blob);
  cv::Mat output = nn_.forward();
  cv::Mat detectionMat(output.size[2], output.size[3], CV_32F,
                       output.ptr<float>());

  trafficLightDetected_.data = false;
  trafficLightSize_.x = 0.0;
  trafficLightSize_.y = 0.0;
  for (int i = 0; i < detectionMat.rows; i++) {
    int class_id = detectionMat.at<float>(i, 1);
    // Ignore other classes provided by model.
    // TODO: Swap last layer to only detect traffic lights
    if (class_id != 10)
      continue;
    float confidence = detectionMat.at<float>(i, 2);

    // Check if the detection is of good enough
    if (confidence > confidenceThreshld_) {
      int x =
          static_cast<int>(detectionMat.at<float>(i, 3) * cvPtr->image.cols);
      int y =
          static_cast<int>(detectionMat.at<float>(i, 4) * cvPtr->image.rows);
      int width = static_cast<int>(
          detectionMat.at<float>(i, 5) * cvPtr->image.cols - x);
      int height = static_cast<int>(
          detectionMat.at<float>(i, 6) * cvPtr->image.rows - y);
      trafficLightDetected_.data = true;
      trafficLightSize_.x = width;
      trafficLightSize_.y = height;
      if (debug_) {
        rectangle(cvPtr->image, cv::Point(x, y),
                  cv::Point(x + width, y + height), cv::Scalar(30, 30, 255), 2);
      }
    }
  }

  if (debug_) {
    cv::imshow("image", cvPtr->image);
    cv::waitKey(5);
  }

  trafficLightDetectedPub_.publish(trafficLightDetected_);
  trafficLightSizePub_.publish(trafficLightSize_);
}

void TrafficLightFetcher::videoCallback(sensor_msgs::ImagePtr msg) {
  frame_ = msg;
}
} // namespace traffic_light_fetcher
