
#include <traffic_light_fetcher/traffic_light_analysis.hpp>

#include <std_msgs/Float32.h>

namespace traffic_light_fetcher {
TrafficLightAnalysis::TrafficLightAnalysis(ros::NodeHandle nh) : nh_{nh} {
  trafficLightDetectedSub_ =
      nh_.subscribe("/traffic_light_detected", 10,
                    &TrafficLightAnalysis::trafficLightDetectedCallback, this);
  trafficLightSizeSub_ =
      nh_.subscribe("/traffic_light_size", 10,
                    &TrafficLightAnalysis::trafficLightSizeCallback, this);

  zoneHeightPub_ = nh_.advertise<std_msgs::Float32>("/zone_height", 10);
}

void TrafficLightAnalysis::step(const ros::TimerEvent &event) {
  if (!trafficLightDetected_.data) {
    return; // No traffi clight detected
  }
  std_msgs::Float32 zHeighMsg;
  zHeighMsg.data = trafficLightSize_->y / 3.f;
  zoneHeightPub_.publish(zHeighMsg);
}

void TrafficLightAnalysis::trafficLightDetectedCallback(std_msgs::Bool msg) {
  trafficLightDetected_ = msg;
}
void TrafficLightAnalysis::trafficLightSizeCallback(
    geometry_msgs::Vector3Ptr msg) {
  trafficLightSize_ = msg;
}
} // namespace traffic_light_fetcher
