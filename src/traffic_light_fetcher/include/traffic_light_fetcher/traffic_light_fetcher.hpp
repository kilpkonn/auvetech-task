#pragma once

#include <geometry_msgs/Vector3.h>
#include <opencv2/dnn.hpp>
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <std_msgs/Bool.h>

namespace traffic_light_fetcher {
class TrafficLightFetcher {
public:
  explicit TrafficLightFetcher(ros::NodeHandle nh);
  void step(const ros::TimerEvent &event);

private:
  ros::NodeHandle nh_;

  ros::Subscriber videoSub_;

  ros::Publisher trafficLightDetectedPub_;
  ros::Publisher trafficLightSizePub_;

  sensor_msgs::ImagePtr frame_;

  std_msgs::Bool trafficLightDetected_;
  geometry_msgs::Vector3 trafficLightSize_;

  cv::dnn::dnn4_v20191202::Net nn_;

  bool debug_;
  double confidenceThreshld_;

  void videoCallback(sensor_msgs::ImagePtr msg);
};
} // namespace traffic_light_fetcher
