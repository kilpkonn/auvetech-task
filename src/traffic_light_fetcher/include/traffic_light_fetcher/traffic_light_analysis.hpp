#pragma once

#include <geometry_msgs/Vector3.h>
#include <ros/ros.h>
#include <std_msgs/Bool.h>

namespace traffic_light_fetcher {
class TrafficLightAnalysis {
public:
  explicit TrafficLightAnalysis(ros::NodeHandle nh);
  void step(const ros::TimerEvent &event);

private:
  ros::NodeHandle nh_;

  ros::Publisher zoneHeightPub_;

  ros::Subscriber trafficLightDetectedSub_;
  ros::Subscriber trafficLightSizeSub_;

  std_msgs::Bool trafficLightDetected_;
  geometry_msgs::Vector3Ptr trafficLightSize_;

  void trafficLightDetectedCallback(std_msgs::Bool msg);
  void trafficLightSizeCallback(geometry_msgs::Vector3Ptr msg);
};
} // namespace traffic_light_fetcher
