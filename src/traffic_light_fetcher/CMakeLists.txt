cmake_minimum_required(VERSION 3.4)
project(traffic_light_fetcher)

if(NOT CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 17)
endif()

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

find_package( OpenCV REQUIRED )
include_directories( ${OpenCV_INCLUDE_DIRS} )


find_package(catkin REQUIRED COMPONENTS
  roscpp
  geometry_msgs
  std_msgs
  roslib
  cv_bridge
)

catkin_package(
  LIBRARIES ${PROJECT_NAME}
  DEPENDS roslib cv_bridge
)

include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

# file(GLOB_RECURSE SOURCE_FILES *.cpp)

# traffic_light_fetcher
add_library(traffic_light_fetcher src/traffic_light_fetcher.cpp)

target_link_libraries(traffic_light_fetcher
  ${catkin_LIBRARIES}
)

add_executable(traffic_light_fetcher_node
  src/traffic_light_fetcher_node.cpp
)

target_link_libraries(traffic_light_fetcher_node
  ${catkin_LIBRARIES}
  traffic_light_fetcher
  ${OpenCV_LIBS}
)

# traffic_light_analysis
add_library(traffic_light_analysis src/traffic_light_analysis.cpp)

add_executable(traffic_light_analysis_node
  src/traffic_light_analysis_node.cpp
)

target_link_libraries(traffic_light_analysis_node
  ${catkin_LIBRARIES}
  traffic_light_analysis
)
